# -*- coding: utf-8 -*-
__revision__ = "$Id$"
__all__ = ['new']

from Random import OSRNG
from Random import _UserFriendlyRNG

def new(*args, **kwargs):
    """Return a file-like object that outputs cryptographically random bytes."""
    return _UserFriendlyRNG.new(*args, **kwargs)

def atfork():
    """Call this whenever you call os.fork()"""
    _UserFriendlyRNG.reinit()

def get_random_bytes(n):
    """Return the specified number of cryptographically-strong random bytes."""
    return _UserFriendlyRNG.get_random_bytes(n)

# vim:set ts=4 sw=4 sts=4 expandtab:
