"""Provides a platform-independent interface to the random number generators
supplied by various operating systems."""

__revision__ = "$Id$"

import os

if os.name == 'posix':
    from Random.OSRNG.posix import new
elif os.name == 'nt':
    from Random.OSRNG.nt import new
elif hasattr(os, 'urandom'):
    from Random.OSRNG.fallback import new
else:
    raise ImportError("Not implemented")

# vim:set ts=4 sw=4 sts=4 expandtab:
